const rolesServices = require("../../services/roles")

module.exports = function () {
    return {
        create,
        update,
        getOneRole,
        getAllRoles,
        delete: deleteRole
    }

    async function create(req, res) {
        try {
            console.log('rolesServices', rolesServices().create)
            const role = await rolesServices().create(req.body)
            res.status(201).json({
                message: 'Successfully created new role.',
                role
            })
        } catch (error) {
            return res.json({
                message: 'Failed to create new role.',
                error
            })
        }
    }

    async function getAllRoles(req, res) {
        try {
            const roles = await rolesServices().get()
            return res.status(200).json({
                message: "Successfully fetched all roles.",
                roles
            })
        } catch (error) {
            return res.json({
                message: 'Failed to get roles.',
                error
            })
        }
    }

    async function getOneRole(req, res) {
        try {
            const role = await rolesServices().get(req.params.roleID)
            return res.status(200).json({
                message: "Successfully fetched role.",
                role
            })
        } catch (error) {
            return res.json({
                message: 'Failed to fetch role.',
                error
            })
        }
    }

    async function update(req, res) {
        try {
            const role = await rolesServices().update(req.params.roleID, req.body)
            res.status(200)
            return res.json({
                message: "Successfully updated role.",
                role
            })
        } catch (error) {
            return res.json({
                message: "Failed to update role.",
                error
            })
        }
    }

    async function deleteRole(req, res) {
        try {
            const role = await rolesServices().delete(req.params.roleID)
            res.status(200)
            return res.json({
                message: "Successfully deleted role.",
                role
            })
        } catch (error) {
            return res.json({
                message: "Failed to delete role.",
                error
            })
        }
    }
}