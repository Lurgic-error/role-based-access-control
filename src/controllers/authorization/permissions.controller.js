const permissionsServices = require("../../services/permissions")

module.exports = function () {
    return Object.freeze({
        create,
        update,
        getOnepermission,
        getAllpermissions,
        delete: deletepermission
    })

    async function create(req, res) {
        try {
            const permission = await permissionsServices().create(req.body)
            res.status(201).json({
                message: 'Successfully created new permission.',
                permission
            })
        } catch (error) {
            return res.json({
                message: 'Failed to create new permission.',
                error
            })
        }
    }

    async function getAllpermissions(req, res) {
        try {
            const permissions = await permissionsServices().get()
            return res.status(200).json({
                message: "Successfully fetched all permissions.",
                permissions
            })
        } catch (error) {
            return res.json({
                message: 'Failed to get permissions.',
                error
            })
        }
    }

    async function getOnepermission(req, res) {
        try {
            const permission = await permissionsServices().get(req.params.permissionID)
            return res.status(200).json({
                message: "Successfully fetched permission.",
                permission
            })
        } catch (error) {
            return res.json({
                message: 'Failed to fetch permission.',
                error
            })
        }
    }

    async function update(req, res) {
        try {
            const permission = await permissionsServices().update(req.params.permissionID, req.body)
            res.status(200)
            return res.json({
                message: "Successfully updated permission.",
                permission
            })
        } catch (error) {
            return res.json({
                message: "Failed to update permission.",
                error
            })
        }
    }

    async function deletepermission(req, res) {
        try {
            const permission = await permissionsServices().delete(req.params.permissionID)
            res.status(200)
            return res.json({
                message: "Successfully deleted permission.",
                permission
            })
        } catch (error) {
            return res.json({
                message: "Failed to delete permission.",
                error
            })
        }
    }
}