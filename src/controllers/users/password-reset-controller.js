module.exports = function passwordResetController({ userServices }){

    return async function reset(req, res){
        try {
            const user = await userServices['reset-password'](req.params.userID, req.body)
            res.status(201).json({
                message:  `Successfully changed password for ${user.fullname}`,
                user
            })
        } catch (error) {
            res.json({
                message: `Failed to change password for user ${req.body.fullname}.`,
                error
            })
        }
    }
}