module.exports = function listUserController({ userServices }){

    return {
        getAll,
        getOne
    }

    async function getAll(req, res){
        try {
            console.log("here \n", await userServices.list())
            let users = await userServices.list()
            console.log('users', users)
            res.status(200).json({
                message: "Successfully fetched all users",
                users
            })

        } catch (error) {
            res.json({
                message: "Failed to get all users.",
                error
            })
        }
    }

    async function getOne(req, res){ 
        try {
            let user = await userServices.list(req.params.userID)
            res.status(201).json({
                message: `Successfully fetched  ${user.fullname}`,
                user,
            })
        } catch (error) {
            res.json({
                message: "Failed to get user.",
                error
            }) 
        }
    }
}