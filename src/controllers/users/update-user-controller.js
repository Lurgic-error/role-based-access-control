module.exports = function UpdateUserController({ userServices }){

    return async function update(req, res){
        try {
            let user = await userServices.update(req.params.userID, req.body)
            res.status(201).json({
                message: `Successfully updated ${user.fullname}.`,
                user
            })
        } catch (error) {
            res.json({
                message: `Failed to update user.`,
                error
            })
        }
    }
}