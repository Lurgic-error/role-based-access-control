const userServices = require('../../services/users')

const registration = require('./registration-controller')

const login = require('./login-controller')

const logout = require('./logout-controller')

const getUserController = require('./get-user-controller')

const updateUserController = require('./update-user-controller')

const deleteUserController = require('./delete-user-controller')

const passwordResetController = require('./password-reset-controller')

const userController = Object.freeze({
    register:registration({ userServices }), 
    login:login({ userServices }),
    logout:logout({ userServices }),
    getAll:getUserController({ userServices }).getAll,
    getOne:getUserController({ userServices }).getOne,
    update:updateUserController({ userServices }),
    delete:deleteUserController({ userServices }),
    passwordReset:passwordResetController({ userServices }),
    "add-role":require("./add-role-controller")({ userServices }),
    "remove-role":require("./remove-role-controller")({ userServices }),
    "grant-permission":require("./grant-permission-controller")({ userServices }),
    "revoke-permission":require("./revoke-permission-controller")({ userServices })
    
})

module.exports = { 
    userController,
    register:registration({ userServices }), 
    login:login({ userServices }),
    logout:logout({ userServices }),
    getAll:getUserController({ userServices }).getAll,
    getOne:getUserController({ userServices }).getOne,
    update:updateUserController({ userServices }),
    delete:deleteUserController({ userServices }),
    passwordReset:passwordResetController({ userServices }),
    "add-role":require("./add-role-controller")({ userServices }),
    "remove-role":require("./remove-role-controller")({ userServices }),
    "grant-permission":require("./grant-permission-controller")({ userServices }),
    "revoke-permission":require("./revoke-permission-controller")({ userServices })
}