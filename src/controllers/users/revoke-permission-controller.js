module.exports = function({ userServices }){

    return async function revoke(req, res){
        try {
            const user = await userServices.permissions.revoke(req.params.userID,req.body)
            return res.status(201).json({
                message:"Successfully revoked permission from user.",
                user
            })
        } catch (error) {
            return res.json({
                message: "Failed to revoke permission from users.",
                error
            })
        }
    }
}