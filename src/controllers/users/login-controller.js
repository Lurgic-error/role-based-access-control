module.exports = function loginController({ userServices }){

    return async function login(req, res){
        try {
            const user = await userServices.login(req.body)
            res.cookie('token', user.token).status(200).json({
                message: `Successfully logged in as  ${user.fullname}`,
                user,
            })
        } catch (error) {
            res.json({
                message: `Failed to login user: ${error.message}. ${error.stack}`,
                error,
            })
        }
    }
}