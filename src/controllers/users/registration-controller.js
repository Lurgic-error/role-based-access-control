module.exports = function userRegistrationController({ userServices }){

    return async function register(req, res){
        try {
            const user = await userServices.create(req.body)
            return res.status(201).json({
                message: 'Successfully created new user.',
                user
            })
        } catch (error) {
            return res.json({
                message: 'Failed to create new user.',
                error,

            })
        }
    }
}