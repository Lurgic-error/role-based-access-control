module.exports = function deleteUserController({ userServices }){

    return async function deleteUser(req, res){
        try {
            let deleted = await userServices.delete(req.params.userID)
            res.status(200).json({
                message: 'User deleted successfully.',
                deleted
            })
        } catch (error) {
            res.status(404).json({
                message: `Failed to delete user.`,
                error
            })
        }
    }
}