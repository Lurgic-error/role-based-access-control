module.exports = function({ userServices }){

    return async function remove(req, res){
        try {
            const user = await userServices.roles.remove(req.params.userID,req.body)
            return res.status(201).json({
                message:"Successfully removed role from user.",
                user
            })
        } catch (error) {
            return res.json({
                message: "Failed to remove role from user.",
                error
            })
        }
    }
}