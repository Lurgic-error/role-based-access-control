module.exports = function logoutController({ userServices }){

    return async function logout(req,res){
            if(await userServices.logout(req.params.userID)){
                return res.status(200).json({
                    message:'User logged out successfully'
                })
            }else{
                return res.status(200).json({
                    message:'User failed to complete logout.'
                })
            }
        }
}