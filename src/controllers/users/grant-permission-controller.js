module.exports = function({ userServices }){

    return async function grant(req, res){
        try {
            const user = await userServices.permissions.grant(req.params.userID,req.body)
            return res.status(201).json({
                message:"Successfully granted user new permission.",
                user
            })
        } catch (error) {
            return res.json({
                message: "Failed to grant user new permission.",
                error
            })
        }
    }
}