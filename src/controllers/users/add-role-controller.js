module.exports = function({ userServices }){

    return async function add(req, res){
        try {
            const user = await userServices.roles.add(req.params.userID,req.body)
            return res.status(201).json({
                message:"Successfully added new role to user.",
                user
            })
        } catch (error) {
            return res.json({
                message: "Failed to add new role to user.",
                error
            })
        }
    }
}
