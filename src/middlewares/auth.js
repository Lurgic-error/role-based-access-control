const jwt = require("jsonwebtoken");
const config = require("config");
const User=require("../models/user.model")

const log =console.log

function authenticate(req, res, next) {
  //check for a cookie labelled token, if present verify token
  // log(req.cookies.token)
  if(req.cookies.token){
    jwt.verify(req.cookies.token,config.get('privatekey'),(err,decoded)=>{
      if(!err){
        //if token is legit find the user that goes by this name and kill him...kidding.
        User.findOne({_id:decoded.id})
      .then(function(user){
        //if there is no user
        if(!user){
          res.send('not allowed!')
        }else{
          //if you find a user
          res.locals.user = user;
          next()
          //res.render('registration/profile',{user:user})
        }
      })}else{
        //if there was an error veryfying the token
        log('there was an error veryfying the token')
        res.send('server error, please <a href="/users/login">log in</a> again')
      }
    })
  }else{
    //if there is no token to be found go to login page
    log('there is no token to be found')
    res.redirect('/users/login')
  }
  
};


const authLevel = {
  one: "admin",
  two: "admin, company",
  three: "admin,company,transport-manager",
  four: "admin,company,transport-manager, yard-manager"
}

function authorize(roles){
  //roles are the values in the constant authLevel
    roles = roles.split(',').map(role=>role.trim())
    return function(req, res, next){
      if (roles.length==0 || !roles.includes(res.locals.user.role)) {
          // user's role is not authorized
          return res.status(401).json({ message: 'Unauthorized' });
      }
    // authentication and authorization successful
    next();
}
}


module.exports = {
  authenticate,
  authorize,
  authLevel
}

