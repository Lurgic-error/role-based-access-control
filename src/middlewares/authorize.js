const db  = require("../models")

module.exports = function(role){

    return async function authorize(req, res, next){
        const roleSelected = await db.Role.findOne({ name:role })
        console.log('req.user', req.user.roles.length, roleSelected)
        if( req.user.roles.length == 0 ){
            return res.send("Not allowed")
        }else{
            req.user.roles.forEach(role => {
                if (role.name == roleSelected.name){
                    return next()
                }else{
                    return res.send("Not allowed.")
                }
            });
        }

    }
}