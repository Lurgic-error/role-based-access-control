require("dotenv").config()
const jwt = require('jsonwebtoken')

const verifyToken = async (req, res, next) =>{
    console.log("Authentication Middleware")
    // Get the auth header value
    const bearerHeader = req.headers['authorization'];
    // check if bearer is undefined
    if(typeof bearerHeader !== 'undefined'){
        // Split at the space
        const bearer = bearerHeader.split(' ');

        // Get token from bearer array
        const bearerToken =  bearer[1]
        // Set the token
        req.token = bearerToken; 
        // Verify token
        const user = await jwt.verify(req.token, process.env.SECRET)
        req.user = user
        // Next middleware
        next();
    }else{
        // Forbidden
        res.sendStatus(403)
    }
}

module.exports = verifyToken;
