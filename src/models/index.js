module.exports = {
    User:require("./user.model"),
    Role:require("./roles.model"),
    Permission:require("./permission.model")
}