const mongoose = require('mongoose');
const Schema  = mongoose.Schema

const Permission = new Schema({

    name:{
        type:String,
        required:true,
        unique:true
    },
    
    actions:[
        {
            type:String
        }
    ]
},{
    timestamps: {
        createdAt: 'created',
        updatedAt: 'modified'
    },
    toJSON: {
        virtuals: true
    },
    strict: false
})

module.exports = mongoose.model('Permission', Permission);