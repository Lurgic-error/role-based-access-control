const mongoose = require('mongoose');
const Schema  = mongoose.Schema
const Role = new Schema({
    name:{
        type:String,
        required:true,
        unique:true
    },
    level:{
        type:Number
    },
    description:{
        type:String
    },
    // resources:[
    //     {
    //         name:{ type: String },
    //         permissions:[{
    //             type:String,
    //             enum:["create", "read", "upate", "delete"]
    //         }]
    //     }
    // ]
},{
    timestamps: {
        createdAt: 'created',
        updatedAt: 'modified'
    },
    toJSON: {
        virtuals: true
    },
    strict: false
})

module.exports = mongoose.model('Role', Role);