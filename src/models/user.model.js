require('dotenv').config()
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const jwt = require('jsonwebtoken')



const User = new Schema({
    name: {
        first: {
            type: String,

        },
        middle: {
            type: String,

        },
        last: {
            type: String,

        }
    },
    username: {
        type: String,
    },
    email: {
        type: String,
        required: [true, 'Email is required.'],
        unique: [true, 'This email is already taken.'],
    },
    password: {
        type: String,
        required: [true, 'Password is required.'],
    },
    dob: {
        type: Date,
    },
    gender: {
        type: String,
        enum: ["male", "female", ]
    },
    mobile: [{
        type: Number,
        default: null
    }],
    roles: [{
        type: Schema.Types.ObjectId,
        ref: 'Role',
        autopopulate: true
    }],
    active: {
        type: Boolean,
        default: false
    },
    verified: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: {
        createdAt: 'created',
        updatedAt: 'modified'
    },
    toJSON: {
        virtuals: true
    },
    strict: false
});

User.virtual('fullname').get(function () {
    return `${this.name.first} ${this.name.last}`
})

User.pre("save", function(next){
    if(!this.username){
        this.username = `${this.name.last}-${this.name.first}-`
    }
    next()
})

User.post("find", function () {
    this.populate("roles")
    this.populate("permission")
})

User.methods.createToken = async function () {
    console.log('this', this)
    this.populate("roles")
    let payload = {
        id: this._id,
        fullname: this.fullname,
        email: this.email,
        roles: this.roles,
        permissions: this.permissions
    }
    let options = {}

    let token = await jwt.sign(payload, process.env.SECRET, options)
    return token
}

User.plugin(require("mongoose-autopopulate"))

module.exports = mongoose.model('User', User);
