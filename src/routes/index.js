const express = require('express');

const router = express.Router();

const users = require('./auth.routes')

const roles = require("./roles.routes")

const permissions = require("./permissions.routes")

router.use('/users', users)

router.use("/roles", roles)

router.use("/permissions", permissions)


router.get('/', function(req,res){
    res.send('welcome to Role Based Access Control API')
})

module.exports = router;