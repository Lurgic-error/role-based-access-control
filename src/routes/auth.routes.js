const express = require('express');

const router = express.Router();

const authenticate = require("../middlewares/verifier")

const authorize = require("../middlewares/authorize")

const { getAll, getOne, register, login, logout, passwordReset, update } = require('../controllers/users/index')

const userController = require('../controllers/users')


router.get('/', getAll);

router.get('/:userID',  getOne);

router.post('/register' , register);

router.post('/login',  login);

router.get('/:userID/logout', logout);

router.put('/:userID/update', update);

router.delete('/:userID/delete', userController.delete);

router.put('/:userID/reset-password', passwordReset);

router.put("/:userID/add-role",  userController["add-role"])

router.put("/:userID/remove-role", authenticate, authorize("admin"), userController["remove-role"])

router.put("/:userID/grant-permission", authenticate, authorize("admin"), userController["grant-permission"])

router.put("/:userID/revoke-permission", authenticate, authorize("admin"), userController["revoke-permission"])

module.exports = router;
