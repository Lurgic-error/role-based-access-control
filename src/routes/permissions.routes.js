const router = require("express").Router()

const permissionsController = require("../controllers/authorization/permissions.controller")

router.get("/", permissionsController().getAllpermissions)

router.get("/:permissionID", permissionsController().getOnepermission)

router.post("/create-permission", permissionsController().create)

router.put("/:permissionID/update", permissionsController().update)

router.delete("/:permissionID", permissionsController().delete)

module.exports = router