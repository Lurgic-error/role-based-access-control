const router = require("express").Router()

const rolesController = require("../controllers/authorization/roles.controller")

router.get("/", rolesController().getAllRoles)

router.get("/:roleID", rolesController().getOneRole)

router.post("/create-role", rolesController().create)

router.put("/:roleID/update", rolesController().update)

router.delete("/:roleID", rolesController().delete)

module.exports = router