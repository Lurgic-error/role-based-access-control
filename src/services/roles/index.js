const { Role } = require("../../models")

module.exports = service = function(){

    return Object.freeze({
        create,
        update,
        get,
        delete:deleteRole
    })

    async function create(data){
        return await Role.create(data)
    }

    async function update(roleID, data){
        const role = await Role.findByIdAndUpdate({
            _id:roleID
        }, data, {new: true})
        if(!role) throw "Uknown role. Are you sure it exists?"
        return role
    }

    async function get(roleID){
        if(roleID){
            return await Role.findById(roleID)
        }else{
            return await Role.find()
        }
    }

    async function deleteRole(roleID){
        const role = await Role.findByIdAndDelete(roleID)
        if(!role) throw "Uknown role. Are you sure it exists?"
        return role
    }
}