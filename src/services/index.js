module.exports = {
    userServices: require("./users"),
    roleServices:require("./roles"),
    permissionServices:require("./permissions")
}