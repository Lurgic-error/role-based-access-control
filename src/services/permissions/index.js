const db = require("../../models")

module.exports = service = function(){

    return Object.freeze({
        create,
        update,
        get,
        delete:deletePermission
    })

    async function create(data){
        return await db.Permission.create(data)
    }

    async function update(permissionID, data){
        const permission = await db.Permission.findByIdAndUpdate({
            _id:permissionID
        }, data, {new: true})
        if(!permission) throw "Uknown permission. Are you sure it exists?"
        return permission
    }

    async function get(permissionID){
        if(permissionID){
            return await db.Permission.findById(permissionID)
        }else{
            return await db.Permission.find()
        }
    }

    async function deletePermission(permissionID){
        const permission = await db.Permission.findByIdAndDelete(permissionID)
        if(!permission) throw "Uknown permission. Are you sure it exists?"
        return permission
    }
}