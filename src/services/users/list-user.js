module.exports = function listUserService({ db }){

    return async function list(userID) {
        console.log("Waza")
        if (!userID) {
            const users = await db.User.find().sort({createdAt:-1}).catch(error => console.error(error))
            return users
        } else {
            const user = await db.User.findById(userID).catch(error => console.error(error))
            if (!user){
                throw `Could not find user with ID ${userID}`
            }
            return user
        }
    }
}