module.exports = function({ db }){

    return{
        add, 
        remove
    }

    async function add(userID, role){
        console.log('role', role)
        const user = await db.User.findById(userID).populate("roles")
        if(!user) throw `User with id ${userID} is not found.`
        user.roles.push(role)
        console.log('user', user)
        return await user.save()
    }

    async function remove(userID, role){
        const user = await db.User.findById(userID).populate("roles")
        if(!user) throw `User with id ${userID} is not found.`
        user.roles.pull(role)
        console.log('user', user)
        return await user.save()
    }
}