module.exports = function changeUserPassword({ db, encryptor }){

    return async function reset(userID, data){
        const user = await db.User.findById(userID)
        if(!user) throw `User with id ${userID} is not found.`
        console.log(await user.updateOne({ password: await encryptPassword(data.password)}));
        console.log('user :>> ', await user.save());
        return await user.save()
    }

    async function encryptPassword(password){
        return await encryptor(password)
    }
}