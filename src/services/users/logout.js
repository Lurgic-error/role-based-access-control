module.exports = function userLogoutService({ db }){

    return async function logout(userID){
        const user = await db.User.findById(userID)
        await user.updateOne({ active: false, token:null })
        await user.save()
        return true
    } 
}