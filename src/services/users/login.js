module.exports = function userLoginService({ db, validator }){

    return async function login({ username, email, password }= {}){
        
        let user = await db.User.findOne({ email: email })
        if(!user) throw `Login failed. Please use correct credentials.`
        if(await validator(password, user.password)){
            let token = await user.createToken()
            await user.updateOne({ active:true, token:token })
            await user.save()
            return user
        }else{
            throw `Login failed. Please use correct credentials.`
        }
        

    }

}