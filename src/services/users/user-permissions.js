module.exports = function({ db }){

    return{
        grant, 
        revoke
    }

    async function grant(userID, permission){
        const user = await db.User.findById(userID)
        if(!user) throw `User with id ${userID} is not found.`
        user.permissions.push(permission)
        return await user.save()
    }

    async function revoke(userID, permission){
        const user = await db.User.findById(userID)
        if(!user) throw `User with id ${userID} is not found.`
        user.permissions.pull(permission)
        return await user.save()
    }
}