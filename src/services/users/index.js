const bcrypt = require('bcrypt')

const db = require('../../models')

const userCreateService = require('./create-user')

const userUpdateService = require('./update-user')

const userDeleteService = require('./delete-user')

const userListService = require('./list-user')

const userLoginService = require('./login')

const userLogoutService = require('./logout')

const userPasswordResetService = require('./password-reset')

const roles = require("./user-roles")

const permissions = require("./user-permissions")

const create = userCreateService({ db, encryptor })

const update = userUpdateService({ db })

const delete_ = userDeleteService({ db })

const list = userListService({ db })

const login = userLoginService({ db, validator })

const logout = userLogoutService({ db })

const reset = userPasswordResetService({ db, encryptor })

async function encryptor(password) {
    try {
        let salt = 10
        return bcrypt.hash(password, salt)
    } catch (error) {
        throw `Failed to create hash.\n${error}`
    }

}

async function validator(password, reference){
    console.log('reference :>> ', reference);
    console.log('password :>> ', password);
    try {
        if(await bcrypt.compare(password, reference)){
            return true
        }else{
            return false
        }
    } catch (error) {
        throw `Password validation: ${error}`
        // throw error
    }
}


module.exports = userServices = Object.freeze({
    create,
    list,
    update,
    delete:delete_,
    login,
    logout,
    'reset-password':reset,
    roles:roles({ db }),
    permissions:permissions({ db })
})

