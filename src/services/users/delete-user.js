module.exports = function deleteUserService({ db }){

    return async function deleteUser(userID){
        const user = await db.User.findByIdAndDelete(userID)
        if(!user) throw `User with id ${userID} is not found.`
        return user
    }
}