module.exports = function createUserService({ db, encryptor  }){

    return async function create(data){
        data.password = await encryptor(data.password)
        let user = await db.User.create(data)
        return user
    }
}