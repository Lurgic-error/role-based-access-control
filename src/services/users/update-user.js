module.exports = function updateUserService({ db }){

    return async function update(userID, data){
        const user = await db.User.findByIdAndUpdate({
            _id: userID
        },
        data, {
            new: true,
            strict: false
        }).populate("roles")
        if(!user) throw `User with id ${userID} is not found.`
        return user
    }
}