const express = require('express');

const cors = require('cors');

const app = express();

const bodyParser = require('body-parser');

const morgan = require('morgan')

const appRoutes = require('./routes/index')

require('dotenv').config()

require('./config/database.config')


// Middleware
app.use(express.static('public'))

// Morgan for logging all requests and responses
app.use(morgan('dev'))

//support parsing of application/x-www-form-urlencoded post data
app.use(bodyParser.urlencoded({
    extended: true
}))

app.use(bodyParser.json())

app.use(cors())

app.use('', appRoutes)


// Error Handler
app.use((req, res, next) => {
    const error = new Error("App error handler: Not found");
    error.status = 404;
    next(error);
})

app.use((error, req, res, next) =>{
    res.status(error.status || 500);
    res.json({
        error:{
            message:error.message
        }
    })
})


const port =  process.env.PORT || 6000;

app.listen(port, () => console.log(`🔥🛡️ Server listening on port: ${port} 🛡️🔥 \nKitu Kimepop`));

